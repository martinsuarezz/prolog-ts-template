import miniProlog from './src/solver';
import {Clause, Predicate} from './src/solver';
import * as fs from 'fs';
const readline = require("readline-sync");

function createPredicateFromString(predicateString: string): Predicate{
    predicateString = predicateString.replace(")", "");
    var predicateParts = predicateString.split("(");
    var predicateName = predicateParts[0]!;
    var args = predicateParts[1]!.split(",");
    return miniProlog.buildPredicate(predicateName, ...args);
}

function createClauseFromString(codeLine: string): Clause{
    var headAndBody = codeLine.split(":-");
    var clauseHead = createPredicateFromString(headAndBody[0]!);
    var clauseBody:Predicate[] = [];
    if (headAndBody.length > 1){
        var bodyPredicates = headAndBody[1]!.split("),");
        for (let predicate of bodyPredicates){
            clauseBody.push(createPredicateFromString(predicate));
        }
    }
    return miniProlog.buildClause(clauseHead, ...clauseBody);
}

function main(){
    var program:Clause[] = [];
    var prologCode = fs.readFileSync('./program.txt','utf8');
    prologCode = prologCode.replace(/(\r\n|\n|\r| )/gm, "");

    var codeLines = prologCode.split('.');
    for (let line of codeLines){
        if (line == "") continue;
        program.push(createClauseFromString(line))
    }

    while (true){
        var command = readline.question(">");
        if (command == "exit") return;
        var query:Predicate = createPredicateFromString(command!);
        if (miniProlog.canProve(program, query)) console.log("SI");
        else console.log("NO");
    }
}

main();