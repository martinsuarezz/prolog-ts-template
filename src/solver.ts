import { MiniProlog } from './MiniProlog';

export type Predicate = {
  name: string
  args: Variable[]
};

export type Clause = {
  head: Predicate,
  body: Predicate[]
};

export type Variable = {
  name: string
  value: string
};


function isVariable(name: string): boolean{
  const initialChar = name.charCodeAt(0);
  return initialChar <= 95 && initialChar >= 65;
}

function buildPredicateArguments(args: string[]): Variable[]{
  let newArgs = [];
  for(let arg of args){
    if (isVariable(arg)) newArgs.push({"name": arg, "value": ""});
    else newArgs.push({"name": arg, "value": arg});
  }
  return newArgs;
}

function getValueFromStack(item: Variable, stack: Variable[]){
  if (item.value != "") return item.value;
  
  for (let variable of stack){
    if (variable.name == item.name) return variable.value;
  }
  return "";
}

function matchVariables(item1: Variable,
                            item2: Variable,
                            stack: Variable[]): boolean{
  
  const item1Value = item1.value;
  const item2Value = getValueFromStack(item2, stack);

  if (item1Value == "" || item2Value == "") return true;
  return item1Value == item2Value;
}

function matchArgsLists(list1: Variable[], 
                        list2: Variable[],
                        stack: Variable[]): boolean{
  if (list1.length != list2.length) return false;
  
  for (let i = 0; i < list1.length; i++){
    if (list1[i] !== undefined && list2[i] !== undefined){
      if (!matchVariables(list1[i]!, list2[i]!, stack)) return false;
    }
  }

  return true;
}

function existsInStack(variable: Variable, stack: Variable[]): boolean{
  for (let stackVariable of stack){
    if (variable.name == stackVariable.name) return true;
  }
  return false;
}

function populateStack(clauseHead: Predicate, predicate: Predicate, stack: Variable[]): Variable[]{
  if (clauseHead.args.length != predicate.args.length) return stack; // No deberia suceder, es un error
  
  let newStack = stack.slice(0);
  let variableName = "";
  let variableValue = "";

  for (let i = 0; i < clauseHead.args.length; i++){
    variableName = clauseHead.args[i]!.name;
    variableValue = predicate.args[i]!.value;
    if (!isVariable(variableName)) continue;
    if (existsInStack(clauseHead.args[i]!, stack)) continue;
    if (variableValue == "") continue;
    
    newStack.push({
      name: variableName,
      value: variableValue
    })
  }

  for (let i = 0; i < predicate.args.length; i++){
    variableName = predicate.args[i]!.name;
    variableValue = clauseHead.args[i]!.value;
    if (!isVariable(variableName)) continue;
    if (existsInStack(clauseHead.args[i]!, stack)) continue;
    if (variableValue == "") continue;
  
    newStack.push({
      name: variableName,
      value: variableValue
    })
  }
  return newStack;
}

export function matchPredicates(predicate1: Predicate, predicate2: Predicate, stack: Variable[]): boolean {
  return (predicate1.name == predicate2.name) && 
          matchArgsLists(predicate1.args, predicate2.args, stack);
}

export function matchClausePredicate(clause: Clause, predicate: Predicate, stack: Variable[]): boolean {
  return matchPredicates(clause.head, predicate, stack);
};

export function arePredicatesTrue(program: Clause[], predicates: Predicate[], stack: Variable[]): boolean{
  if (predicates.length == 0) return true;

  let newStack = stack.slice(0);
  for (let clause of program){
    newStack = stack.slice(0);
    if (matchClausePredicate(clause, predicates[0]!, newStack)){
      newStack = populateStack(clause.head, predicates[0]!, newStack);
      if (arePredicatesTrue(program, clause.body.concat(predicates.slice(1)), newStack))
        return true;
    }
  }
  return false;
}

const miniProlog: MiniProlog<Clause, Predicate> = {
  buildPredicate: (name: string, ...args: string[]): Predicate => {
    return {
      "name": name,
      "args": buildPredicateArguments(args)
    };
  },

  buildClause: (head: Predicate, ...body: Predicate[]): Clause => {
    return {
      "head": head,
      "body": body
    };
  },

  canProve: (program: Clause[], query: Predicate): boolean => {
    return arePredicatesTrue(program, [query], []);
  }
};

export default miniProlog;
