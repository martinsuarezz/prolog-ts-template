import miniProlog from '../src/solver';
import {matchPredicates, matchClausePredicate, Variable,
    arePredicatesTrue} from '../src/solver';

describe('clauses', function() {
    const clause1 = miniProlog.buildClause(
        miniProlog.buildPredicate('parent', 'alice', 'bob')
    );

    const clause2 = miniProlog.buildClause(
        miniProlog.buildPredicate('parent', 'alice', 'bob')
    );

    const clause3 = miniProlog.buildClause(
        miniProlog.buildPredicate('parent', 'alice', 'charlie')
    );

    it('equality', () => {
        expect(clause1).toEqual(clause2);
    });

    it('non-equality', () => {
        expect(clause1).not.toEqual(clause3);
    });
});

describe('predicates', function() {
    const predicate1 = miniProlog.buildPredicate('parent', 'alice', 'bob');
    const predicate2 = miniProlog.buildPredicate('parent', 'alice', 'bob');
    const predicate3 = miniProlog.buildPredicate('parent', 'alice', 'charlie');
    const predicate4 = miniProlog.buildPredicate('parent', 'alice', 'X');
    const predicate5 = miniProlog.buildPredicate('parent', 'X', 'alice');

    let emptyStack: Variable[];
    emptyStack = [];

    it('equality', () => {
        expect(predicate1).toEqual(predicate2);
    });

    it('non-equality', () => {
        expect(predicate1).not.toEqual(predicate3);
    });

    it('matches', () => {
        expect(matchPredicates(predicate1, predicate2, emptyStack)).toBeTruthy();
    });

    it('not matches', () => {
        expect(matchPredicates(predicate1, predicate3, emptyStack)).toBeFalsy();
    });

    it('matches with variable', () => {
        expect(matchPredicates(predicate1, predicate4, emptyStack)).toBeTruthy();
    });

    it('not matches with variable', () => {
        expect(matchPredicates(predicate1, predicate5, emptyStack)).toBeFalsy();
    });
});

describe('predicates and clauses', function() {
    const predicate1 = miniProlog.buildPredicate('parent', 'alice', 'bob');
    const clause1 = miniProlog.buildClause(
        miniProlog.buildPredicate('parent', 'alice', 'bob')
    );

    let emptyStack: Variable[];


    it('predicate matches clause', () => {
        expect(matchClausePredicate(clause1, predicate1, emptyStack)).toBeTruthy();
    });
});

describe('predicates and program stack', function() {
    const relatives = [
        miniProlog.buildClause(
          miniProlog.buildPredicate('parent', 'alice', 'bob')
        ),
        miniProlog.buildClause(
          miniProlog.buildPredicate('parent', 'alice', 'charlie'),
        ),
        miniProlog.buildClause(
          miniProlog.buildPredicate('parent', 'bob', 'dave')
        ),
        miniProlog.buildClause(
          miniProlog.buildPredicate('grandparent', 'Grandparent', 'Grandchild'),
          miniProlog.buildPredicate('parent', 'Grandparent', 'Middle'),
          miniProlog.buildPredicate('parent', 'Middle', 'Grandchild'),
        ),
    ];

    const predicate1 = miniProlog.buildPredicate('parent', 'alice', 'bob');
    const predicate2 = miniProlog.buildPredicate('parent', 'alice', 'dave');
    const predicate3 = miniProlog.buildPredicate('parent', 'alice', 'X');

    let emptyStack: Variable[];
    emptyStack = []

    it('predicate is true', () => {
        expect(arePredicatesTrue(relatives, [predicate1], emptyStack)).toBeTruthy();
    });

    it('predicate is false', () => {
        expect(arePredicatesTrue(relatives, [predicate2], emptyStack)).toBeFalsy();
    });

    it('predicate with free variable is true', () => {
        expect(arePredicatesTrue(relatives, [predicate3], emptyStack)).toBeTruthy();
    });

    let newStack: Variable[];
    newStack = [{
        name: "X",
        value: "dave"
    }]

    it('predicate with variable values is false', () => {
        expect(arePredicatesTrue(relatives, [predicate3], newStack)).toBeFalsy();
    });

    let newStack2: Variable[];
    newStack2 = [{
        name: "X",
        value: "bob"
    }]

    it('predicate with variable values is true', () => {
        expect(arePredicatesTrue(relatives, [predicate1], newStack2)).toBeTruthy();
    });

    let newStack3: Variable[];
    newStack3 = [{
        name: "X",
        value: "alice"
    },
    {
        name: "Y",
        value: "bob"
    }]

    const predicate4 = miniProlog.buildPredicate('parent', 'X', 'Y');

    it('predicate with variables values is true', () => {
        expect(arePredicatesTrue(relatives, [predicate4], newStack3)).toBeTruthy();
    });

    let newStack4: Variable[];
    newStack4 = [{
        name: "A",
        value: "alice"
    },
    {
        name: "B",
        value: "bob"
    },
    {
        name: "X",
        value: "bob"
    },
    {
        name: "Y",
        value: "dave"
    },
    ]

    const predicate5 = miniProlog.buildPredicate('parent', 'A', 'B');

    it('multiple predicates with variables values is true', () => {
        expect(arePredicatesTrue(relatives, [predicate4, predicate5], newStack4)).toBeTruthy();
    });

    let newStack5: Variable[];
    newStack5 = [{
        name: "A",
        value: "alice"
    },
    {
        name: "B",
        value: "bob"
    },
    {
        name: "X",
        value: "alice"
    },
    {
        name: "Y",
        value: "dave"
    },
    ];

    it('multiple predicates with variables values is false', () => {
        expect(arePredicatesTrue(relatives, [predicate4, predicate5], newStack5)).toBeFalsy();
    });

    const predicate6 = miniProlog.buildPredicate('parent', 'Grandparent', 'Middle');
    const predicate7 = miniProlog.buildPredicate('parent', 'Middle', 'Grandchild');

    let newStack6: Variable[];
    newStack6 = [{
        name: "Grandparent",
        value: "alice"
    },
    {
        name: "Grandchild",
        value: "bob"
    }
    ];

    it('multiple predicates of familiar relationships with variables values is false', () => {
        expect(arePredicatesTrue(relatives, [predicate6, predicate7], newStack6)).toBeFalsy();
    });
    
});
